=== Hey Clippy ===

Contributors: czedonis
Tags: codeChallenge
Requires at least: 4.7
Tested up to: 5.4
Stable tag: 4.3
Requires PHP: 7.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Clippy's back to annoy you once again in popup form. Write your own hints or advice snippets for him to display.

== Description ==

A basic little plugin that puts clippy on your front end when users are logged in and displays a randomized 'advice' post
on page load. 

== Frequently Asked Questions ==

= Why? =

Why not! Nostalgia and a reason to make a plugin from scratch.

= Will this ever be a real think? =

Probably not, though I expect I'll keep messing with it for a bit in the future.


