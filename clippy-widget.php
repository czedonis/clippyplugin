<?php
//
///**
// * @package ClippyWidget
// * @version 1.1.1
// */
///*
//Plugin Name: Hey Clippy!
//Description: Clippy's back! He's here to help. Add a popup with helpful info to your front end pages for user who are
//logged in
//Author: Christine 'Cat' Zedonis
//Version: 1.1.0
//
//*/

if (!defined('ABSPATH')) exit;

if (!class_exists('ClippyWidget'))
{
    /**
     * Class ClippyWidget
     */
    class ClippyWidget
    {
        private $custom_post_type = 'clippyAdvice';
        private $file_base;

        /**
         * ClippyWidget constructor.
         */
        public function __construct()
        {
            $this->file_base = plugin_dir_path(dirname(__FILE__)) . 'clippy-widget/clippy-widget.php';

            register_activation_hook($this->file_base, array(&$this, 'activate_plugin'));
            register_deactivation_hook($this->file_base, array(&$this, 'deactivate_plugin'));

            add_action('init',[&$this, 'create_advice_post_type']);
            add_action('wp_enqueue_scripts', [&$this, 'load_css_js'],99);

            add_filter('the_post', [&$this, 'load_clippy_advice']);
            add_filter('plugin_action_links_' . plugin_basename(__FILE__), [&$this, 'add_plugin_settings_link']);

        }

        /**
         * flush rewrite rules on activation
         */
        public function activate_plugin()
        {
            flush_rewrite_rules();
        }

        /**
         * create custom post type to hold our advice, only uses Title and Excerpt
         */
        public function create_advice_post_type()
        {
            $result = $this->register_advice_post_type();
            if(!is_wp_error($result))
                $this->set_default_advice_post();
        }

        /**
         * load js and necessary dependency and css files
         */
        public function load_css_js()
        {
            wp_enqueue_script('jquery');
            wp_enqueue_style( 'clippy_css', plugin_dir_url(__FILE__) . '/src/includes/css/clippy.css', false, NULL, 'screen' );
            wp_enqueue_script('clippy_js', plugin_dir_url(__FILE__) . '/src/includes/js/clippy.js', array('jquery'), null, false);
        }

        /**
         * registers CLippy Advice post type
         * @return WP_Error|WP_Post_Type
         */
        public function register_advice_post_type()
        {

            $labels = array(
                'name' => _x('Clippy Advice', 'Post Type General Name'),
                'singular_name' => _x('Clippy Advice', 'Post Type Singular Name'),
                'menu_name' => __('Clippy Advice'),
                'name_admin_bar' => __('Clippy Advice'),
                'all_items' => __('All Clippy Advice'),
                'add_new_item' => __('Add New Clippy Advice'),
                'add_new' => __('Add Clippy Advice'),
                'new_item' => __('New Clippy Advice'),
                'edit_item' => __('Edit Clippy Advice'),
                'update_item' => __('Update Clippy Advice'),
                'view_item' => __('View Clippy Advice'),
                'search_items' => __('Search Clippy Advice'),
                'not_found' => __('Clippy has no advice to give'),
                'not_found_in_trash' => __('Clippy Advice Not found in Trash'),
            );

            $rewrite = array(
                'slug' => $this->custom_post_type,
                'with_front' => false,
                'pages' => true,
                'feeds' => false,
            );

            $args = array(
                'label' => __($this->custom_post_type),
                'description' => __('Displays Clippy Advice Information'),
                'labels' => $labels,
                'supports' => array('title', 'excerpt'),
                'public' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'menu_position' => 20,
                'menu_icon' => 'dashicons-format-status',
                'show_in_admin_bar' => true,
                'show_in_nav_menus' => true,
                'can_export' => false,
                'has_archive' => false,
                'exclude_from_search' => true,
                'rewrite' => $rewrite,
                'publicly_queryable' => false,
                'capability_type' => 'post'
            );

            return register_post_type($this->custom_post_type, $args);
        }

        /**
         * if clippyAdvice post type is successfully created, this adds the default post
         */
        public function set_default_advice_post()
        {
            if (is_null(get_page_by_title('Welcome to Hey Clippy', OBJECT, $this->custom_post_type))) {
                $default_advice = array(
                    'post_type' => $this->custom_post_type,
                    'post_title' => 'Welcome to Hey Clippy',
                    'post_excerpt' => 'Clippy is here to help! Provide random info or advice to your logged in users.',
                    'post_status' => "publish",
                    'post_author' => 1
                );
                wp_insert_post($default_advice);
            }
        }

        /**
         * adds direct settings link to plugins page when active
         * @param $links
         * @return mixed
         */
        public function add_plugin_settings_link($links)
        {
            $settings_link = '<a href="edit.php?post_type=clippyadvice">Settings</a>';
            array_unshift($links, $settings_link);
            return $links;
        }

        /**
         * pulls in front end widget
         */
        public function load_clippy_advice()
        {
            include_once('src/partials/clippy_advice.php');
        }

        /**
         * deletes all clippy posts on deactivation of plugin, flushes rewrite rules
         * and dequeues unneeded scripts
         */
        public function deactivate_plugin()
        {
            global $wpdb;
            wp_dequeue_style('clippy_css');
            wp_dequeue_script('clippy_js');
            $wpdb->delete($wpdb->posts, ['post_type' => $this->custom_post_type]);
            unregister_post_type( $this->custom_post_type );
            flush_rewrite_rules();
        }

    }
}
new ClippyWidget();

