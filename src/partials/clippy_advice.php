<?php

global $post;
//only show clippy if you're logged in and on a front facing post/page
if(is_user_logged_in() && !is_admin()) {
    $the_query = new WP_Query(array('post_type' => 'clippyAdvice', 'orderby' => 'rand', 'posts_per_page' => '1'));

    if ($the_query->have_posts()) :
        while ($the_query->have_posts()) :
            $clip = $the_query->the_post();
            $current_user = wp_get_current_user();

            ?>
            <div id="clipWrap">
                <div id="clippyBox">
                    <div id="clippyTextBubble">
        <span id="titleBar">
        <?php echo $current_user->first_name . ', '; ?>
            <?php echo get_the_title($clip); ?>
            <span id="closeAdvice">X</span>
        </span>
                        <span id="clippyAdviceContent">
            <?php echo get_the_excerpt($clip); ?>
        </span>
                    </div>
                    <div id="imageWrap">
                        <img src="<?php echo plugin_dir_url(__FILE__) . '../assets/clippysm.png'; ?>" />
                    </div>
                </div>
            </div>
        <?php
        endwhile;
    endif;
    wp_reset_query();
}